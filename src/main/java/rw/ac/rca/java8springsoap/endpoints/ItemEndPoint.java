package rw.ac.rca.java8springsoap.endpoints;

import jaxb.classes.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import rw.ac.rca.java8springsoap.repositories.IItemRepository;
import rw.ac.rca.java8springsoap.models.Item;

import java.util.List;
import java.util.Optional;

@Endpoint
public class ItemEndPoint {
    private final IItemRepository itemRepository;

    @Autowired
    public ItemEndPoint(IItemRepository repository) {
        this.itemRepository = repository;
    }

    @PayloadRoot(namespace = "https://rca.ac.rw/peace/soap-app", localPart = "NewItemRequest")
    @ResponsePayload
    public NewItemResponse create(@RequestPayload NewItemRequest dto) {
        jaxb.classes.Item __item = dto.getItem();

        Item _item = mapItem(__item);

        Item item = itemRepository.save(_item);

        NewItemResponse response = new NewItemResponse();

        __item.setId(item.getId());

        response.setItem(__item);

        return response;
    }

    @PayloadRoot(namespace = "https://rca.ac.rw/peace/soap-app", localPart = "GetAllItemRequest")
    @ResponsePayload
    public GetAllItemResponse findAll(@RequestPayload GetAllItemRequest request) {

        List<Item> item = itemRepository.findAll();

        GetAllItemResponse response = new GetAllItemResponse();

        for (Item item : item) {
            jaxb.classes.Item _item = mapItem(item);

            response.getItem().add(_item);
        }

        return response;
    }

    @PayloadRoot(namespace = "https://rca.ac.rw/peace/soap-app", localPart = "GetItemDetailsRequest")
    @ResponsePayload
    public GetItemDetailsResponse findById(@RequestPayload GetItemDetailsRequest request) {
        Optional<Item> _item= itemRepository.findById(request.getId());

        if (!_item.isPresent())
            return new GetItemDetailsResponse();

        Item item = _item.get();

        GetItemDetailsResponse response = new GetItemDetailsResponse();

        jaxb.classes.Item __item = mapItem(item);

        response.setItem(__item);

        return response;
    }

    @PayloadRoot(namespace = "https://rca.ac.rw/peace/soap-app", localPart = "DeleteItemRequest")
    @ResponsePayload
    public DeleteItemResponse delete(@RequestPayload DeleteItemRequest request) {
        itemRepository.deleteById(request.getId());
        DeleteItemResponse response = new DeleteItemResponse();
        response.setMessage("Successfully deleted an item");
        return response;
    }

    @PayloadRoot(namespace = "https://rca.ac.rw/peace/soap-app", localPart = "UpdateItemRequest")
    @ResponsePayload
    public UpdateItemResponse update(@RequestPayload UpdateItemRequest request) {
        jaxb.classes.Item __item = request.getItem();

        Item _item = mapItem(__item);
        _item.setId(__item.getId());

        Item item = itemRepository.save(_item);

        UpdateItemResponse itemDTO = new UpdateItemResponse();

        __item.setId(item.getId());

        itemDTO.setItem(__item);

        return itemDTO;
    }
    private jaxb.classes.Item mapItem(Item item) {
        jaxb.classes.Item item = new jaxb.classes.Item();
        _item.setId(item.getId());
        _item.setName(item.getName());
        _item.setItemCode(item.getItemCode());
        _item.setStatus(item.getStatus());
        _item.setPrice(item.getPrice());
        _item.setSupplier(item.getSupplier());
        return _item;
    }

    private Item mapItem(jaxb.classes.Item __item) {
        return new Item(__item.getId(), __item.getName(), __item.getItemCode(), __item.getStatus(), __item.getPrice(), __item.getSupplier());
    }
}
