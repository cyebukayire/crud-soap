package rw.ac.rca.java8springsoap.endpoints;
import jaxb.classes.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import rw.ac.rca.java8springsoap.repositories.IStatusRepository;
import rw.ac.rca.java8springsoap.models.Status;

import java.util.List;
import java.util.Optional;

    @Endpoint
    public class StatusEndPoint {
        private final IStatusRepository statusRepository;

        @Autowired
        public StatusEndPoint(IStatusRepository repository) {
            this.statusRepository = repository;
        }

        @PayloadRoot(namespace = "https://rca.ac.rw/peace/soap-app", localPart = "NewStatusRequest")
        @ResponsePayload
        public NewStatusResponse create(@RequestPayload NewStatusRequest dto) {
            jaxb.classes.Status __status = dto.getStatus();

            Status _status = mapStatus(__status);

            Status status = statusRepository.save(_status);

            NewStatusResponse response = new NewStatusResponse();

            __status.setId(status.getId());

            response.setStatus(__status);

            return response;
        }

        @PayloadRoot(namespace = "https://rca.ac.rw/peace/soap-app", localPart = "GetAllStatusRequest")
        @ResponsePayload
        public GetAllStatusResponse findAll(@RequestPayload GetAllStatusRequest request) {

            List<Status> status = statusRepository.findAll();

            GetAllStatusResponse response = new GetAllStatusResponse();

            for (Status status : status) {
                jaxb.classes.Status _status = mapStatus(status);

                response.getStatus().add(_status);
            }

            return response;
        }

        @PayloadRoot(namespace = "https://rca.ac.rw/peace/soap-app", localPart = "GetStatusDetailsRequest")
        @ResponsePayload
        public GetStatusDetailsResponse findById(@RequestPayload GetStatusDetailsRequest request) {
            Optional<Status> _status= statusRepository.findById(request.getId());

            if (!_status.isPresent())
                return new GetStatusDetailsResponse();

            Status status = _status.get();

            GetStatusDetailsResponse response = new GetStatusDetailsResponse();

            jaxb.classes.Status __status = mapStatus(status);

            response.setStatus(__status);

            return response;
        }

        @PayloadRoot(namespace = "https://rca.ac.rw/peace/soap-app", localPart = "DeleteStatusRequest")
        @ResponsePayload
        public DeleteStatusResponse delete(@RequestPayload DeleteStatusRequest request) {
            statusRepository.deleteById(request.getId());
            DeleteStatusResponse response = new DeleteStatusResponse();
            response.setMessage("Successfully deleted an status");
            return response;
        }

        @PayloadRoot(namespace = "https://rca.ac.rw/peace/soap-app", localPart = "UpdateStatusRequest")
        @ResponsePayload
        public UpdateStatusResponse update(@RequestPayload UpdateStatusRequest request) {
            jaxb.classes.Status __status = request.getStatus();

            Status _status = mapStatus(__status);
            _status.setId(__status.getId());

            Status status = statusRepository.save(_status);

            UpdateStatusResponse statusDTO = new UpdateStatusResponse();

            __status.setId(status.getId());

            statusDTO.setStatus(__status);

            return statusDTO;
        }
        private jaxb.classes.Status mapStatus(Status status) {
            jaxb.classes.Status status = new jaxb.classes.Status();
            _status.setId(status.getId());
            _status.setName(status.getName());
            return _status;
        }

        private Status mapStatus(jaxb.classes.Status __status) {
            return new Status(__status.getId(), __status.getName();
        }
    }
