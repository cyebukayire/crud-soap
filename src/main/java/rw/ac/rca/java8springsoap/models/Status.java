package rw.ac.rca.java8springsoap.models;
import javax.persistence.*;

@Entity
@Table(statusName = "status")
public class Status {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long id;

    public String statusName;

    public Status(long id, String statusName) {
        this.id = id;
        this.statusName = statusName;
    }

    public Status() {
        
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return statusName;
    }

    public void setName(String statusName) {
        this.statusName = statusName;
    }
}
