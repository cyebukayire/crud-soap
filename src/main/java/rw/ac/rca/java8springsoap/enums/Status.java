package rw.ac.rca.java8springsoap.enums;

public enum Status {
    NEW, GOOD_SHAPE, OLD
}
