package rw.ac.rca.java8springsoap.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;


@EnableWs //Enable Spring Web Services
@Configuration //Spring Configuration
public class WebServiceConfig {

    // MessageDispatcherServlet
    // ApplicationContext
    // url -> /ws/*

    @Bean
    public ServletRegistrationBean<MessageDispatcherServlet> messageDispatcherServlet(ApplicationContext context) {
        MessageDispatcherServlet messageDispatcherServlet = new MessageDispatcherServlet();
        messageDispatcherServlet.setApplicationContext(context);
        messageDispatcherServlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean<>(messageDispatcherServlet, "/ws/peace/*");
    }

    // /ws/peace/item.wsdl
    @Bean(name = "item")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema itemSchema) {
        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setPortTypeName("ItemPort");
        definition.setTargetNamespace("https://rca.ac.rw/peace/soap-app");
        definition.setLocationUri("/ws/peace");
        definition.setSchema(itemSchema);
        return definition;
    }

    // /ws/peace/supplier.wsdl
    @Bean(name = "supplier")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema supplierSchema) {
        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setPortTypeName("SupplierImport");
        definition.setTargetNamespace("https://rca.ac.rw/peace/soap-app");
        definition.setLocationUri("/ws/peace");
        definition.setSchema(supplierSchema);
        return definition;
    }

    // /ws/peace/status.wsdl
    @Bean(name = "status")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema statusSchema) {
        DefaultWsdl11Definition definition = new DefaultWsdl11Definition();
        definition.setPortTypeName("StatusImport");
        definition.setTargetNamespace("https://rca.ac.rw/peace/soap-app");
        definition.setLocationUri("/ws/peace");
        definition.setSchema(statusSchema);
        return definition;
    }

    @Bean
    public XsdSchema itemSchema() {
        return new SimpleXsdSchema(new ClassPathResource("app.xsd"));
    }
}

